=========================
CentralNode documentation
=========================

This project is developing the CentralNode (Mid and Low) component of the Telescope Monitoring and Control (TMC) prototype, for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   getting_started/getting_started
   



Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
